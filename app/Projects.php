<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
     protected $table = 'projects';
    protected $fillable = [
        'judul'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
