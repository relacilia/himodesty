<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Storage;
use File;
use App\Projects;
use App\Likes;
use App\User;
use Image;

class GeneralController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allProjects = Projects::get();
        foreach($allProjects as $key => $value)
        {
             $value->user->name;
             $value->sum = Likes::where('id_gambar','=',$value->id)->count();
        }
        return view('beranda')->with([
                    'listProjects'     => $allProjects
        ]);
    }

    public function getAssetImg($id,$nama_asset, $pixel) {
        $gbr = Projects::where('id', '=', $id)->first();
        $path = $gbr->path;
        $pathGambar = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . $path;
        if(!File::exists($pathGambar)) {
            $pathGambar = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().'../image_404.png';
        }

        $gambarRequested = Image::make($pathGambar);
        $gambarRequested->resize($pixel, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        return $gambarRequested->response($gambarRequested->mime());
    }

    public function show_profil($id){
        $listproject = Projects::where('user_id','=', $id)->get();
        $user = User::where('id','=', $id)->get();
        foreach($listproject as $key => $value)
        {
             $value->user->name;
             $value->sum = Likes::where('id_gambar','=',$value->id)->count();
        }
        return view('general.profil')->with([
                    'listProjects'     => $listproject,
                    'user'             => $user
        ]);
    }

}
