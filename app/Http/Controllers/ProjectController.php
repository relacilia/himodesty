<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Storage;
use File;
use App\Projects;
use Image;
use Response;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function view_upload()
    {
        return view('upload');
    }

    public function do_upload(Request $request)
    {   
        $judul = $request->input('judul');
        $newImageProject = $request->input('image-data');

        $contentNewImage    = file_get_contents($newImageProject);
        $sizeNewImage       = strlen(base64_decode($newImageProject));
        $typeNewImage       = mime_content_type($newImageProject);
        $nameFileNewImage   = substr(hash('sha512',Carbon::now()->toDayDateTimeString()), -8).'_'.$this->toFileName($judul).'.'.explode('/',$typeNewImage)[1];
        $pathNewImage       = 'project-images/'.Auth::user()->email.'/'.$nameFileNewImage;

        $gambar             = new Projects;
        $gambar->path       = $pathNewImage;
        $gambar->image_name = $nameFileNewImage;
        $gambar->image_mime = $typeNewImage;
        $gambar->image_size = $sizeNewImage;
        $gambar->judul      = $judul;
        $gambar->deskripsi  = $request->input('desc');
        $gambar->user_id    = Auth::user()->id;
        $gambar->save();
        Storage::put($pathNewImage, $contentNewImage);

        return redirect(Auth::user()->username);
    }

    private function toFileName($data) {
        return preg_replace('/[^a-zA-Z0-9-]/', '', $data);
    }
}
