<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\Services\SocialAccountService;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/upload';
    // Dirubah pada vendor\laravel\framework\src\Illuminate\Foundation\Auth\RedirectsUsers.php
    //tambahkan : 
    //use Illuminate\Support\Facades\Auth;
    //ganti return paling bawah dengan ini :
    //return property_exists($this, 'redirectTo') ? $this->redirectTo : Auth::user()->username.'/upload';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $socialService = new SocialAccountService;
        //notice we are not doing any validation, you should do it
        //$user = Socialite::driver($provider)->user();
        $user = $socialService->createOrGetUser(Socialite::driver($provider));

        // Data From Provider 
        // $idUserFromProvider = $user->getId();
        // $nicknameUserFromProvider = $user->getNickname();
        // $avatarUserFromProvider =  $user->getAvatar();
        // $nameUserFromProvider = $user->getName();
        // $emailUserFromProvider = $user->getEmail();

        // stroing data to our use table and logging them in
        // $data = [
        //     'name' => $nameUserFromProvider,
        //     'email' => $emailUserFromProvider,
        //     'username' => $nicknameUserFromProvider
        // ];
     
        Auth::login($user);

        //after login redirecting to home page
        return redirect($this->redirectPath());
    }
}
