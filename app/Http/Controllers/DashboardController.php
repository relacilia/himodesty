<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use Storage;
use File;
use App\Projects;
use App\User;
use App\Likes;
use App\Follow;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    //Menampilkan project/fortofolio dari user
    public function index()
    {
        $user_id = Auth::user()->id;
        $project_user = Projects::where('user_id', '=', $user_id)->get();
         foreach($project_user as $key => $value)
        {
             $value->user->name;
             $value->sum = Likes::where('id_gambar','=',$value->id)->count();
        }
        return view('dashboard')->with([
                    'listProjects'     => $project_user
        ]);
    }   

    public function like_project($id){
        $id_user = Auth::user()->id;
        $liking = new Likes;
        $liking->id_user = $id_user;
        $liking->id_gambar = $id;
        $liking->save();

        return redirect(Auth::user()->username);
    } 

    public function follow ($id){
        $id_user = Auth::user()->id;
        $follow = new Follow;
        $follow->follower = $id_user;
        $follow->following = $id;
        $follow->save();
        return redirect(Auth::user()->username);
    } 

    public function view_edit_profil (){
        $id_user = Auth::user()->id;
        $user = User::where('id','=', $id_user)->first();

         return view('profil.edit_profile')->with([
                    'user'     => $user
        ]);
    } 
}
