<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'jenis_user';
    protected $fillable = [
        'nama'
    ];
}
