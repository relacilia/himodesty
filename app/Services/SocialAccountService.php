<?php

namespace App\Services;

use Laravel\Socialite\Contracts\Provider;
use App\SocialAccount;
use App\User;
class SocialAccountService
{
    public function createOrGetUser(Provider $provider)
    {

        $providerUser = $provider->user();
        $providerName = class_basename($provider); 

        $account = SocialAccount::whereProvider($providerName)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $providerName
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                // Data From Provider 
                $idUserFromProvider = $providerUser->getId();
                $nicknameUserFromProvider = $providerUser->getNickname();
                $avatarUserFromProvider =  $providerUser->getAvatar();
                $nameUserFromProvider = $providerUser->getName();
                $emailUserFromProvider = $providerUser->getEmail();
                if(!$nicknameUserFromProvider) {
                    $nicknameUserFromProvider = explode('@', $emailUserFromProvider)[0];
                    // dd($nicknameUserFromProvider);
                    // $nicknameUserFromProvider = str_replace(" ", '', $nicknameUserFromProvider);
                }
                $user = User::create([
                    'name' => $nameUserFromProvider,
                    'email' => $emailUserFromProvider,
                    'username' => $nicknameUserFromProvider
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}