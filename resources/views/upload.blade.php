@extends('layouts.app')

@section('template_css')
<style>
    .cropit-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 2px dashed rgba(34, 34, 34, .1);
        border-radius: 3px;
        margin-top: 7px;
        width: 34rem;
        height: 34rem;
        margin-left: auto;
        margin-right: auto;
    }

    .cropit-preview:hover {
        border-color: rgba(238,43,49, .5);
    }

    .cropit-preview-image-container {
        cursor: move;
    }

    .image-size-label {
        margin-top: 10px;
    }

    input {
        display: block;
    }

    /* Translucent background image */
    .cropit-preview-background {
        opacity: 0;
    }

    /*
    * If the slider or anything else is covered by the background image,
    * use relative or absolute position on it
    */
    input.cropit-image-zoom-input {
        position: relative;
        display: inline-block;
        width: 70%;
    }

    /* Limit the background image by adding overflow: hidden */
    #image-cropper {
        overflow: hidden;
    }

</style>
@endsection

@section('template_js')
    
@endsection

@section('content')
<div class="container main">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-himo">
                <div class="panel-heading">
                    <i class="material-icons middle">camera</i>
                    <span class="middle">New Project</span>
                </div>
                <div class="panel-body">
                    <form role="form" data-toggle="validator" class="form-horizontal" method="POST" action="{{ route('project.store', [Auth::user()->username]) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('files') ? ' has-error' : '' }}">
                            <div class="col-sm-6 col-sm-offset-3 center">
                                <!--<div class="fileUpload btn himo" style="display: block">Upload
                                    <input onchange="readURL(this);" id="files" type="file" class="upload" name="files" value="{{ old('files') }}" required autofocus>
                                </div>-->

                                <div class="image-editor">
                                    <!--<input type="file" class="cropit-image-input">-->
                                    <div class="fileUpload btn grey-light" style="display: block; text-align: left">Cari gambar...
                                        <input id="files" type="file" class="cropit-image-input upload" name="files" value="{{ old('files') }}" required autofocus>
                                    </div>
                                    <div class="spinner"><div class="spinner-dot"></div><div class="spinner-dot"></div><div class="spinner-dot"></div></div>
                                    <div class="cropit-preview" style="margin-bottom: 1rem"></div>
                                    <!--<div class="image-size-label">
                                        Resize image
                                    </div>-->
                                    <i class="material-icons inline">zoom_out</i>
                                        <input type="range" class="cropit-image-zoom-input inline">
                                    <i class="material-icons inline">zoom_in</i>
                                    <i class="material-icons inline rotate-cw-btn pointer">rotate_right</i>
                                    <input type="hidden" name="image-data" class="hidden-image-data" />
                                </div>

                                <script>
                                    $(function() {
                                        $('.image-editor').cropit({ 
                                            imageState: {
                                                src: '{{ URL::asset('img/favicon.png') }}'
                                            },
                                            smallImage: 'allow',
                                            onImageLoading: function() {
                                                $('.spinner').show();
                                            },
                                            onImageLoaded: function() {
                                                $('.spinner').hide();
                                            },
                                            onImageError: function(object) {
                                                alert(object.message);
                                            }
                                        });

                                        // Handle rotation
                                        $('.rotate-cw-btn').click(function() {
                                            $('.image-editor').cropit('rotateCW');
                                        });
                                        $('.rotate-ccw-btn').click(function() {
                                            $('.image-editor').cropit('rotateCCW');
                                        });

                                        $('form').submit(function() {
                                            // Move cropped image data to hidden input
                                            var imageData = $('.image-editor').cropit('export');
                                            $('.hidden-image-data').val(imageData);

                                            // Print HTTP request params
                                            //var formValue = $(this).serialize();
                                            //$('#result-data').text(formValue);

                                            // Prevent the form from actually submitting
                                            // return false;
                                        });
                                    });
                                </script>

                                @if ($errors->has('files'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('files') }}</strong>
                                    </span>
                                @else
                                    <span class="help-block info center">
                                        <span style="font-family: 'Roboto'; font-weight: 500">
                                            Max Size: 5MB | Tipe Gambar: jpg, png, jpeg
                                        </span>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label>Judul</label>
                            <input class="form-control" placeholder="Enter title" name="judul">
                        </div>-->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-8 col-md-offset-2">
                                <!--<label for="judul" class="control-label">Nama Portofolio</label>-->
                                <input id="judul" type="text" class="form-control" name="judul" value="{{ old('judul') }}" placeholder="Nama Portofolio" required>

                                @if ($errors->has('judul'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('judul') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
                            <div class="col-md-8 col-md-offset-2">
                                <!--<label for="desc" class="control-label">Deskripsi Portofolio</label>-->
                                <textarea class="form-control" name="desc" rows="3" placeholder="Deskripsi Portofolio"></textarea>
                                @if ($errors->has('desc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('desc') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <!--<button type="submit" class="btn btn-full himo">SIMPAN </button>-->
                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-full himo">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
