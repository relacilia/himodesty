@extends('layouts.app')

@section('content')
<div class="container main">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-himo">
                <div class="panel-heading">Edit Profil</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="name" class="control-label">Name</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="username" class="control-label">Username</label>
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('bio') ? ' has-error' : '' }}">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="bio" class="control-label">Biografi</label>
                                <input id="bio" type="text" class="form-control" name="bio" value="{{ old('bio') }}" required autofocus>
                                @if ($errors->has('bio'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('bio') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('work_at') ? ' has-error' : '' }}">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="work_at" class="control-label">Work at</label>
                                <input id="work_at" type="text" class="form-control" name="work_at" value="{{ old('work_at') }}" required autofocus>
                                @if ($errors->has('work_at'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('work_at') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('work_exp') ? ' has-error' : '' }}">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="work_exp" class="control-label">Work Experience</label>
                                <input id="work_exp" type="text" class="form-control" name="work_exp" value="{{ old('work_exp') }}" required autofocus>
                                @if ($errors->has('work_exp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('work_exp') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('achievement') ? ' has-error' : '' }}">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="achievement" class="control-label">Achievement</label>
                                <input id="achievement" type="text" class="form-control" name="achievement" value="{{ old('achievement') }}" required autofocus>
                                @if ($errors->has('achievement'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('achievement') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="email" class="control-label">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="password" class="control-label">Password</label>
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="password-confirm" class="control-label">Confirm Password</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-full himo">
                                    Daftar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
