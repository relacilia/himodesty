@extends('layouts.app')

@section('template_fonts')
    <!-- Font Awesome -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
@endsection

@section('template_css')
    <!-- Pixel Fabric clothes icons -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('grid-item/fonts/pixelfabric-clothes/style.css') }}" />
	<!-- Flickity gallery styles -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('grid-item/css/flickity.css') }}" />
	<!-- Component styles -->
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('grid-item/css/component.css') }}" />
@endsection

@section('template_js')
    <script src="{{ URL::asset('grid-item/js/modernizr.custom.js') }}"></script>
@endsection

@section('content')
<div id="beranda-slide" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <!--<li data-target="#myCarousel" data-slide-to="0" class="active"></li>-->
        <!--<li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>-->
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <!--<img src="{{ URL::asset('img/background1.jpg') }}" alt="Himodesty">-->
            <div class="carousel-caption" style="font-family: 'Droid Serif', serif;">
                <h2 style="font-weight: 300">buat</h2>
                <h1 style="font-weight: 400; font-size: 6rem; margin-top: 5px">Portofolio</h1>
                <h1 style="font-weight: 400; font-size: 6rem; margin-top: 0">Onlinemu!</h1>
                @if (Auth::guest())
                    <a type="button" class="btn himo" data-toggle="modal" data-target="#loginModal">buat sekarang</a>
                @else
                    <a href="{{ route('project.upload', [Auth::user()->username]) }}" type="button" class="btn himo">buat sekarang</a>
                @endif
            </div>
        </div>

        <!--<div class="item">
        <img src="{{ URL::asset('img/background1.jpg') }}" alt="Himodesty">
        </div>

        <div class="item">
        <img src="{{ URL::asset('img/background1.jpg') }}" alt="Himodesty">
        </div>

        <div class="item">
        <img src="{{ URL::asset('img/background1.jpg') }}" alt="Himodesty">
        </div>-->
    </div>
    <!-- Left and right controls -->
    <!--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>-->
</div>
<script>$('#beranda-slide .carousel-inner').parallax({imageSrc: '{{ URL::asset('img/background1.jpg') }}', naturalHeight: 1000});</script>

<div class="container p-top" style="min-height: 100vh">
    <!-- Bottom bar with filter and cart info -->
	<div class="bar" style="display:none">
		<div class="filter">
			<span class="filter__label">Filter: </span>
			<button class="action filter__item filter__item--selected" data-filter="*">All</button>
			<button class="action filter__item" data-filter=".jackets"><i class="icon icon--jacket"></i><span class="action__text">Jackets</span></button>
			<button class="action filter__item" data-filter=".shirts"><i class="icon icon--shirt"></i><span class="action__text">Shirts</span></button>
			<button class="action filter__item" data-filter=".dresses"><i class="icon icon--dress"></i><span class="action__text">Dresses</span></button>
			<button class="action filter__item" data-filter=".trousers"><i class="icon icon--trousers"></i><span class="action__text">Trousers</span></button>
			<button class="action filter__item" data-filter=".shoes"><i class="icon icon--shoe"></i><span class="action__text">Shoes</span></button>
		</div>
		<button class="cart">
			<i class="cart__icon fa fa-shopping-cart"></i>
			<span class="text-hidden">Shopping cart</span>
			<span class="cart__count">0</span>
		</button>
	</div>
	<!-- Main view -->
	<div class="view">
		<!-- Grid -->
		<section class="grid grid--loading">
			<!-- Loader -->
			<img class="grid__loader" src="{{ URL::asset('grid-item/images/grid.svg') }}" width="60" alt="Loader image" />
			<!-- Grid sizer for a fluid Isotope (Masonry) layout -->
			<div class="grid__sizer"></div>
            
			<!-- Grid items -->
            @foreach($listProjects as $key => $value)
			<div class="grid__item ">
                <div class="meta">
                    <!-- <span class="meta__like">
                        <a href="{{ url('/login') }}" style="font-weight: 500">
                            <i class="material-icons middle">favorite_border</i>
                        </a>
                    </span> -->
                </div> 
				<div class="images">
                    <img src="{{url('asset/img/'.$value->id.'/'.$value->image_name.'/300')}}" style="width:205px">
                </div>
				<div class="meta">
					<h3 class="meta__title">{{$value->judul}}</h3>
<!--                     <span class="meta__price shadow2p">559K</span> -->
					<span class="meta__user">
                        <a href="{{ url('/'.$value->user_id.'/profil') }}" style="font-weight: 500">
                            <i class="material-icons middle">account_circle</i>
                            <span class="middle">{{$value->user['name']}}</span>
                        </a>
                    </span>
                   <!--  <span class="meta__user">
                       <fieldset class="rating">
                            <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                        </fieldset>
                    </span> -->
                    <span class="meta__user pull-right" style="line-height: 24px; padding-right: 5px">
                        <a href="{{ url('/'.$value->id.'/like') }}" style="font-weight: 500">
                            <i class="material-icons middle" style="font-size: 2.0rem;">favorite_border</i>
                        </a>
                        <span class="middle">{{$value->sum}}</span>
                    </span>
				</div>
				<button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="text-hidden">Add to cart</span></button>
			</div>
            @endforeach
		</section>
		<!-- /grid-->
	</div>
	<!-- /view -->
    
	<script src="{{ URL::asset('grid-item/js/isotope.pkgd.min.js') }}"></script>
	<script src="{{ URL::asset('grid-item/js/flickity.pkgd.min.js') }}"></script>
	<script src="{{ URL::asset('grid-item/js/main.js') }}"></script>
</div>
@endsection

@section('footer')
    
@endsection
