<div class="modal animated fadeIn" id="loginModal" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 450px">
		<div class="modal-content">
			<div class="modal-body center" style="padding: 1em 6em">
                <h3>
                    <img src="{{ URL::asset('img/favicon.png') }}" alt="Logo Himodesty" style="width: 100px">
                </h3>
                <h4>
                    Selamat datang di HiModesty!
                </h4>
                <p>Buat Portofolio Online dan raih kesempatan mewujudkan Desainmu disini!</p>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" style="margin-top: 3rem">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <!--<label for="email" class="col-md-4 control-label">E-Mail Address</label>-->
                        <div class="col-md-12">
                            <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                            @if ($errors->has('password'))
                                
                            @else
                                <span class="help-block">
                                    <a href="{{ url('/password/reset') }}">Lupa Password?</a>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            {!! Form::button('<span class="middle"> Lanjutkan</span>', array('class' => 'btn btn-full himo','type' => 'submit')) !!}
                            <a class="btn btn-full grey-light m-top1" style="color: rgba(34, 34, 34, .6)" href="{{ url('/register') }}">Daftar</a>
                            <p style="padding: 0; margin: 1.5rem 0 1rem 0;">atau masuk dengan</p> 
                            <a class="btn-dark btn-full blue" href="{{ route('social.login', ['facebook']) }}">Facebook</a>
                        </div>
                    </div>
                </form>
			</div>
            <div class="modal-footer">
            </div>
		</div>
	</div>
</div>