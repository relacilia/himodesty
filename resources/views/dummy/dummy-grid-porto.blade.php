<div class="grid__item ">
    <div class="meta">
        <span class="meta__like">
            <a href="{{ url('/login') }}" style="font-weight: 500">
                <i class="material-icons middle">favorite_border</i>
            </a>
        </span>
    </div> 
    <div class="slider">
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-blue.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-peach.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-white.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
    </div>
    <div class="meta">
        <h3 class="meta__title">Jasmine 1</h3>
        <span class="meta__price shadow2p">559K</span>
        <span class="meta__user">
            <a href="{{ url('/login') }}" style="font-weight: 500">
                <i class="material-icons middle">account_circle</i>
                <span class="middle">HiModesty</span>
            </a>
        </span>
        <span class="meta__user pull-right" style="line-height: 24px; padding-right: 5px">
            <a style="font-weight: 500" >
                <i class="material-icons middle" style="font-size: 1.5rem">favorite</i>
                <span class="middle">45</span>
            </a>
        </span>
    </div>
    <button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="text-hidden">Add to cart</span></button>
</div>

<div class="grid__item ">
    <div class="meta">
        <span class="meta__like">
            <a href="{{ url('/login') }}" style="font-weight: 500">
                <i class="material-icons middle">favorite_border</i>
            </a>
        </span>
    </div> 
    <div class="slider">
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-blue.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-peach.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-white.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
    </div>
    <div class="meta">
        <h3 class="meta__title">Jasmine 1</h3>
        <span class="meta__price shadow2p">559K</span>
        <span class="meta__user">
            <a href="{{ url('/login') }}" style="font-weight: 500">
                <i class="material-icons middle">account_circle</i>
                <span class="middle">HiModesty</span>
            </a>
        </span>
        <span class="meta__user pull-right" style="line-height: 24px; padding-right: 5px">
            <a style="font-weight: 500" >
                <i class="material-icons middle" style="font-size: 1.5rem">favorite</i>
                <span class="middle">45</span>
            </a>
        </span>
    </div>
    <button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="text-hidden">Add to cart</span></button>
</div>

<div class="grid__item ">
    <div class="meta">
        <span class="meta__like">
            <a href="{{ url('/login') }}" style="font-weight: 500">
                <i class="material-icons middle">favorite_border</i>
            </a>
        </span>
    </div> 
    <div class="slider">
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-blue.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-peach.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-white.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
    </div>
    <div class="meta">
        <h3 class="meta__title">Jasmine 1</h3>
        <span class="meta__price shadow2p">559K</span>
        <span class="meta__user">
            <a href="{{ url('/login') }}" style="font-weight: 500">
                <i class="material-icons middle">account_circle</i>
                <span class="middle">HiModesty</span>
            </a>
        </span>
        <span class="meta__user pull-right" style="line-height: 24px; padding-right: 5px">
            <a style="font-weight: 500" >
                <i class="material-icons middle" style="font-size: 1.5rem">favorite</i>
                <span class="middle">45</span>
            </a>
        </span>
    </div>
    <button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="text-hidden">Add to cart</span></button>
</div>

<div class="grid__item ">
    <div class="meta">
        <span class="meta__like">
            <a href="{{ url('/login') }}" style="font-weight: 500">
                <i class="material-icons middle">favorite_border</i>
            </a>
        </span>
    </div> 
    <div class="slider">
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-blue.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-peach.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-white.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
    </div>
    <div class="meta">
        <h3 class="meta__title">Jasmine 1</h3>
        <span class="meta__price shadow2p">559K</span>
        <span class="meta__user">
            <a href="{{ url('/login') }}" style="font-weight: 500">
                <i class="material-icons middle">account_circle</i>
                <span class="middle">HiModesty</span>
            </a>
        </span>
        <span class="meta__user pull-right" style="line-height: 24px; padding-right: 5px">
            <a style="font-weight: 500" >
                <i class="material-icons middle" style="font-size: 1.5rem">favorite</i>
                <span class="middle">45</span>
            </a>
        </span>
    </div>
    <button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="text-hidden">Add to cart</span></button>
</div>

<div class="grid__item ">
    <div class="meta">
        <span class="meta__like">
            <a href="{{ url('/login') }}" style="font-weight: 500">
                <i class="material-icons middle">favorite_border</i>
            </a>
        </span>
    </div> 
    <div class="slider">
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-blue.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-peach.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
        <div class="slider__item" style="background: url({{ URL::asset('img/contoh-produk/j-white.jpg') }}) center no-repeat white; height: 100%; background-size: 100% auto;"></div>
    </div>
    <div class="meta">
        <h3 class="meta__title">Jasmine 1</h3>
        <span class="meta__price shadow2p">175K</span>
        <span class="meta__user">
            <a href="{{ url('/login') }}" style="font-weight: 500">
                <i class="material-icons middle">account_circle</i>
                <span class="middle">HiModesty</span>
            </a>
        </span>
        <span class="meta__user pull-right" style="line-height: 24px; padding-right: 5px">
            <a style="font-weight: 500" >
                <i class="material-icons middle" style="font-size: 1.5rem">favorite</i>
                <span class="middle">45</span>
            </a>
        </span>
    </div>
    <button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="text-hidden">Add to cart</span></button>
</div>