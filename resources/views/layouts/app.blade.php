<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="">
    <meta property="og:url" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="{{ URL::asset('img/favicon.png') }}">
    <meta property="og:type" content="website">

    <link rel="icon" type="image/png" href="{{ URL::asset('img/favicon.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if (trim($__env->yieldContent('judul_halaman')))@yield('judul_halaman') | @endif Himodesty</title>

    <!-- Fonts -->
    {{-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"> --}}
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600" rel="stylesheet" type="text/css">
    @yield('template_fonts')

    <!-- Styles -->
    <!--<link href="/css/app.css" rel="stylesheet">-->
    <link rel="stylesheet" href="{{ URL::asset('css/reset.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/style-setting.css') }}" />
    
    @yield('template_css')

    <!-- Scripts -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="{{ URL::asset('js/jquery.cropit.js') }}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{ URL::asset('js/stellar.js') }}"></script>
    @yield('template_js')
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-fixed-top shadow2p">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" role="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false" aria-controls="app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}" style="padding: 5px 15px; padding-bottom: 5px">
                        <img src="{{ URL::asset('img/logo/top.png') }}" alt="Logo Himodesty" style="height: 100%">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav" style="font-weight: 500">
                        {{-- <li>
                            <a href="{{ url('/') }}" style="font-weight: 500">
                                <i class="material-icons middle">home</i>
                                <span class="middle" style="line-height: 1px">home</span>
                            </a>
                        </li> --}}
                        <li>
                            <a href="{{ url('/') }}" style="font-weight: 500">
                                <i class="material-icons middle">search</i>
                                <span class="middle" style="line-height: 1px">explore</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/') }}" style="font-weight: 500">
                                <i class="material-icons middle">camera_enhance</i>
                                <span class="middle" style="line-height: 1px">challange</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/') }}" style="font-weight: 500">
                                <i class="material-icons middle">shopping_basket</i>
                                <span class="middle" style="line-height: 1px">order</span>
                            </a>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li>
                                <a href="#" style="font-weight: 500" data-toggle="modal" data-target="#loginModal">
                                    <i class="material-icons middle">camera</i>
                                    <span class="middle" style="line-height: 1px">upload portofoliomu</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" style="font-weight: 500" data-toggle="modal" data-target="#loginModal">
                                    <i class="material-icons middle">face</i>
                                    <span class="middle" style="line-height: 1px">login</span>
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="{{ route('project.upload', [Auth::user()->username]) }}" style="font-weight: 500">
                                    <i class="material-icons middle">camera</i>
                                    <span class="middle" style="line-height: 1px">upload portofoliomu</span>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="min-width: 160px">
                                    <div class="image-square inline" style="height: 24px;">
                                        <img src="{{ URL::asset('img/favicon.png') }}"> 
                                    </div>
                                    <span class="middle" style="line-height: 1px">{{ Auth::user()->name }}</span>
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('profile.edit', [Auth::user()->username]) }}">
                                            <i class="material-icons middle">face</i>
                                            <span class="middle" style="line-height: 1px">Profile</span>
                                        </a>
                                    </li>
                                     <li>
                                        <a href="{{ route('profile.change.password', [Auth::user()->username]) }}">
                                            <i class="material-icons middle">face</i>
                                            <span class="middle" style="line-height: 1px">Change Password</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="material-icons middle">exit_to_app</i>
                                            <span class="middle" style="line-height: 1px">Logout</span>
                                        </a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
        @if (Auth::guest())
            @include('modals.login')
            @include('modals.login-script')
        @endif
        @yield('footer')
    </div>
</body>
</html>
