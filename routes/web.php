<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'GeneralController@index');

Auth::routes();
Route::get('/login', function() {
    abort('404');
});

Route::get('social/login/redirect/{provider}', ['uses' => 'Auth\LoginController@redirectToProvider', 'as' => 'social.login']);
Route::get('social/login/{provider}', 'Auth\LoginController@handleProviderCallback');

Route::get('/{username}', ['uses' => 'DashboardController@index', 'as' => 'profile.show']);
Route::get('/{username}/profile/edit', ['uses' => 'DashboardController@view_edit_profil', 'as' => 'profile.edit']);
Route::get('/{username}/profile/change-password', ['uses' => 'DashboardController@view_edit_profil', 'as' => 'profile.change.password']);

Route::get('/{username}/project/upload', ['uses' => 'ProjectController@view_upload', 'as' => 'project.upload']);
Route::post('/{username}/project/store', ['uses' => 'ProjectController@do_upload', 'as' => 'project.store']);

Route::get('/{id}/like', ['uses' => 'DashboardController@like_project', 'as' => 'project.like']);
Route::get('/{id}/profil', ['uses' => 'GeneralController@show_profil', 'as' => 'profil']);
Route::get('/{id}/following', ['uses' => 'DashboardController@follow', 'as' => 'follow.user']);

Route::get('asset/img/{id}/{nama_asset}/{pixel}', ['uses' => 'GeneralController@getAssetImg', 'as' => 'project.img']);
Route::get('images/{filename}', function ($filename)
{
    return Image::make(storage_path() . '/' . $filename)->response();
});
